---
title: Auteur
metadata:
    description: 'Exemple de page auteur qui peut aussi servir à présenter un CV'
slug: a-propos
editable-simplemde:
    self: false
---

# À propos de moi
Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam.

[section name=education]
### Formation

- [2013](#)
  Lorem ipsum.  
  Sed ut perspiciatis unde omnis 
- [2010](#)
  Lorem ipsum.  
  Sed ut perspiciatis unde omnis 
- [2005](#)
  Lorem ipsum.  
  Sed ut perspiciatis unde omnis  
- [2000](#)
  Lorem ipsum.  
  Sed ut perspiciatis unde omnis 
[/section]

[section name=experience]
### Expérience

- [2015](#)
  Graphisme et Web.  
  Sed ut perspiciatis unde omnis
- [2014](#)
  Graphisme et Web.  
  Sed ut perspiciatis unde omnis
- [2013](#)
  Direction artistique 
  Sed ut perspiciatis unde omnis
[/section]

[section name=skills]
### Compétences

[g-progressbar label="PHP" type="success" value="80" min="0" max="100" render=true][/g-progressbar]
[g-progressbar label="MySQL" type="success" value="80" min="0" max="100" render=true][/g-progressbar]
[g-progressbar label="JavaScript" type="success" value="90" min="0" max="100" render=true][/g-progressbar]
[g-progressbar label="jQuery" type="success" value="90" min="0" max="100" render=true][/g-progressbar]
[g-progressbar label="CSS" type="success" value="90" min="0" max="100" render=true][/g-progressbar]
[g-progressbar label="Gimp" type="warning" value="60" min="0" max="100" render=true][/g-progressbar]
[g-progressbar label="Git" type="success" value="80" min="0" max="100" render=true][/g-progressbar]
[/section]
